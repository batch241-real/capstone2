const jwt = require('jsonwebtoken');

const secret = 'Capstone API'

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	// jwt.sign(data/payload, secretkey, options)
	return jwt.sign(data, secret, {});
}


// token verification

module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization;

	if(typeof token !== 'undefined'){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {
			
			if(error){
				return res.send({auth: "failed"});
			} else{
				next();
			}

		})

	} else{
		
		return res.send({auth: "failed"});
	}
}

// token decryption
module.exports.decode = (token) => {
	if(typeof token !== 'undefined'){
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (error, data) => {

			if(error){
				return null
			} else{
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	} else{
		return null;
	}
}