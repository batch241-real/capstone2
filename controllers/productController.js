const User = require('../models/User');
const Product = require('../models/Product')

module.exports.addProduct = (data, reqBody) => {
	return User.findById(data.userId).then(result => {
		if(!result.isAdmin){
			return false
		} else{
			let newProduct = new Product({
				productName: reqBody.productName,
				description: reqBody.description,
				price: reqBody.price
			})

			return newProduct.save().then((product, error) => {
				if(error){
					return false
				} else {
					return true
				}
			})
		}
	})
}

module.exports.allProducts = (data) => {
	return User.findById(data.userId).then(user => {
		if(!user.isAdmin){
			return false;
		} else{
			return Product.find({}).then(product => {
				return product
			})
		}
	})
}

module.exports.activeProduct = () => {
	return Product.find({isActive:true}).then(product => {
		return product
	})
}

module.exports.singleProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(product => {
		if(product.isActive){
			return product;
		} else{
			return false;
		}
	})
}

module.exports.updateProduct = (data) => {
	return User.findById(data.userId).then(user => {
		if(!user.isAdmin){
			return false;
		} else{

			let updateProduct = {
				productName: data.productName,
				description: data.description,
				price: data.price
			}

			return Product.findByIdAndUpdate(data.productId, updateProduct).then((product,error) => {
				if(error){
					return false
				} else{
					return true
				}
			})
		}
	})
	
}

module.exports.archiveProduct = (data) => {
	return User.findById(data.userId).then(user => {
		if(!user.isAdmin){
			return false;
		} else{
			return Product.findById(data.productId).then(product => {
				let archiveProduct

				if(product.isActive){
					archiveProduct = {isActive: false}
				} else {
					archiveProduct = {isActive: true}
				}

				return Product.findByIdAndUpdate(data.productId, archiveProduct).then((product,error) => {
					if(error){
						return false
					} else{
						return true
					}
				})
			})
		}
	})
}