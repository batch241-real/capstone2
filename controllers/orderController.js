const Order = require('../models/Order');
const User = require('../models/User');
const Product = require('../models/Product');
const Cart = require('../models/Cart');

module.exports.getUserOrders = (data) => {
	return User.findById(data.userId).then(user => {
		return Order.find({userId:data.userId}).then(order => {
			return order;
		});
	});
}

module.exports.getAllOrders = (data) => {
	return User.findById(data.userId).then(user => {
		if(user.isAdmin){
			return Order.find({}).then(order => {
				return order;
			});
		} else{
			return false
		}
	});
}

module.exports.addToCart = (data) => {
	return User.findById(data.userId).then(user => {
		if(data.quantity === 0){
			return false
		} else{
			return Product.findById(data.productId).then(product => {
				let addToCart = new Cart({
					userId: data.userId,
					email: user.email,
					productId: data.productId,
					productName: product.productName,
					quantity: data.quantity,
					subTotal: product.price * data.quantity
				});

				return addToCart.save().then((cart, error) => {
					if(error){
						return false
					} else{
						return cart
					}
				});
			});
		}

	});
}

module.exports.changeQuantity = (data) => {
	return User.findById(data.userId).then(user => {
		if(!user){
			return false
		} else{
		return Cart.findById(data.cartId).then(cart => {
				cart.subTotal = (cart.subTotal/cart.quantity) * data.quantity
				cart.quantity = data.quantity;
			return cart.save().then((cart,error) => {
				if(error){
					return false
				} else{
					return true
				}
			})
		})}
	})
}

module.exports.removeToCart = (data) => {
	return User.findById(data.userId).then(user => {
		// let removeToCart = {isAdded:false}
		return Cart.findByIdAndDelete(data.cartId).then((cart,error) => {
			if(error){
				return false;
			} else{
				return true;
			}
		})
	})
}

module.exports.getSubTotal = (data) => {
	return User.findById(data.userId).then(user => {
		return Cart.aggregate([{$match:{$and:[{isAdded:true},{productId:data.productId}]}},{$group: {"_id": "$email", "subtotal amount":{"$sum": "$subTotal"}}},{$project: {"_id":0}}]).then(cart => {
			return cart
		})
	})
	
}

module.exports.getTotalAmount = (data) => {
	return User.findById(data.userId).then(user => {
		if(data.userId){
			return Cart.aggregate([{$match:{$and:[{isAdded:true},{userId:data.userId}]}}, {$group: {"_id": "$email", "total amount":{"$sum": "$subTotal"}}}, {$project: {"_id":0}}]).then(cart => {
				return cart
			})
		}
	})
}

module.exports.retrieveCart = (data) => {
	return User.findById(data.userId).then(user => {
		return Cart.find({userId:data.userId}).then(cart => {
			return cart
		})
	})
}