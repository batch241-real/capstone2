const bcrypt = require('bcrypt');
const User = require('../models/User');
const auth = require('../auth');
const Product = require('../models/Product');
const Cart = require('../models/Cart');
const Order = require('../models/Order');

module.exports.registerUser = (reqBody) => {
	return User.findOne({email:reqBody.email}).then(result => {
		if(result !== null){
			return false
		} else{
			let newUser = new User ({
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10)
			});

			return newUser.save().then((user, error) => {
				if(error){
					return false;
				} else{
					return true
				}
			})
		}
	})	
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email:reqBody.email}).then(result => {
		if(result === null){
			return false
		} else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			} else{
				return false
			}
		}
	})
}


module.exports.userUpdate = (data) => {
	return User.findById(data.adminId).then(result => {
		if(!result.isAdmin){
			return false;
		} else{
			return User.findById(data.userId).then(user => {
				let updateUser
				
				if(user.isAdmin){
					updateUser = { isAdmin: false }
				} else{
					updateUser = { isAdmin: true }
				}

				return User.findByIdAndUpdate(data.userId, updateUser).then((user,error) => {
					if(error){
						return false
					} else{
						return true
					}
				})
			})
		}
	})
}


module.exports.retrieveUser = (data) => {
	return User.findById(data.userId).then(result => {
		return result;
	})
}

module.exports.checkOutCart = (data) => {
	return User.findById(data.userId).then(user => {
		if(data.quantity === 0){
			return false
		} else{
			return Cart.findById(data.cartId).then(cart => {
				if(!cart.isAdded){
					return false
				} else{
					let newOrder = new Order({
						userId: cart.userId,
						email: user.email,
						products: [{
							productId: cart.productId,
							productName: cart.productName,
							quantity: cart.quantity
						}],
						totalAmount: cart.subTotal * cart.quantity
					});

					return newOrder.save().then((order, error) => {
						if(error){
							return false
						} else{
							return Cart.findByIdAndDelete(data.cartId).then((fromCart, error) => {
								if (error){
									return false
								} else{
									return true
								}
							})
						}
					})
				}
			})
		}
	})
}

module.exports.checkOutOrder = (data) => {
	return User.findById(data.userId).then(user => {
		if(data.quantity === 0){
			return false
		} else{
			return Product.findById(data.productId).then(product => {
				let newOrder = new Order({
					userId: user._id,
					email: user.email,
					products: [{
						productId: data.productId,
						productName: product.productName,
						quantity: data.quantity
					}],
					totalAmount: product.price * data.quantity
				});

				return newOrder.save().then((order, error) => {
					if(error){
						return false
					} else{
						return true
					}
				})
			})
		}
	})
}

module.exports.getAllUsers = (data) => {
	return User.findById(data.userId).then(user => {
		if(!user.isAdmin){
			return false
		} else{
			return User.find({}).then(user => {
				return user
			})
		}
	})
}