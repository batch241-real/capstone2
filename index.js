const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const userRoute = require('./routes/userRoute');
const productRoute = require('./routes/productRoute');
const orderRoute = require('./routes/orderRoute')

const port = 4000;

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.set('strictQuery', false);
mongoose.connect('mongodb+srv://admin123:admin123@cluster0.ug17qel.mongodb.net/Capstone2-API?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('error', console.error.bind('Connection Error'));
db.once('open', () => console.log('Connected to the Atlas'));


app.use('/users', userRoute);
app.use('/products', productRoute);
app.use('/orders', orderRoute);

app.listen(port, () => {
	console.log(`API is now online on port ${port}`);
})