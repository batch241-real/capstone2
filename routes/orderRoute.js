const express = require('express');
const router = express.Router();

const auth = require('../auth');
const orderController = require('../controllers/orderController');

// retrieve auth user orders
router.get('/usersorder', auth.verify, (req,res) => {
	let data = {userId: auth.decode(req.headers.authorization).id}
	orderController.getUserOrders(data).then(resultFromController => res.send(resultFromController));
});

// retrieve all orders by admin
router.get('/allorders', auth.verify, (req,res) => {
	let data = {userId: auth.decode(req.headers.authorization).id}
	orderController.getAllOrders(data).then(resultFromController => res.send(resultFromController));
})

// add to cart
router.post('/addtocart', auth.verify, (req,res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity
	}
	orderController.addToCart(data).then(resultFromController => res.send(resultFromController));
});

// change quantities
router.patch('/quantity', auth.verify, (req,res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		cartId: req.body.cartId,
		quantity: req.body.quantity
	}
	orderController.changeQuantity(data).then(resultFromController => res.send(resultFromController));
});

// remove to cart
router.patch('/removetocart', auth.verify, (req,res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		cartId: req.body.cartId
	}
	orderController.removeToCart(data).then(resultFromController => res.send(resultFromController));
});

// subtotal
router.post('/subtotal/:productId', auth.verify, (req,res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.params.productId
	}
	orderController.getSubTotal(data).then(resultFromController => res.send(resultFromController));
});


// total
router.get('/totalamount', auth.verify, (req,res) => {
	let data = {userId: auth.decode(req.headers.authorization).id}
	orderController.getTotalAmount(data).then(resultFromController => res.send(resultFromController));
});


// cart
router.get('/cart', auth.verify, (req,res) => {
	let data = {userId: auth.decode(req.headers.authorization).id}
	orderController.retrieveCart(data).then(resultFromController => res.send(resultFromController));
})

module.exports = router;