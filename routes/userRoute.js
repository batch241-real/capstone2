const express = require('express');
const auth = require('../auth')
const router = express.Router()

const userController = require('../controllers/userController')

// route for register users
router.post('/register', (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


// route for auth login users
router.post('/login', (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


// checkout order
router.post('/checkoutcart', auth.verify, (req,res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		cartId: req.body.cartId
	}
	userController.checkOutCart(data).then(resultFromController => res.send(resultFromController));
});

// checkout order
router.post('/checkout', auth.verify, (req,res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity
	}
	userController.checkOutOrder(data).then(resultFromController => res.send(resultFromController));
});


// set user by admin
router.patch('/updateuser/:userId', auth.verify, (req,res) => {
	let data = {
		adminId: auth.decode(req.headers.authorization).id,
		userId: req.params.userId
	}
	userController.userUpdate(data).then(resultFromController => res.send(resultFromController));
});


// retrieve user details
router.get('/userdetails', auth.verify, (req,res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id
	}
	userController.retrieveUser(data).then(resultFromController => res.send(resultFromController));
});


// all users
router.get('/allusers', auth.verify, (req,res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id
	}
	userController.getAllUsers(data).then(resultFromController => res.send(resultFromController));
})

module.exports = router;