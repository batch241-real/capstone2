const express = require('express');
const router = express.Router();
const auth = require('../auth');

const productController = require('../controllers/productController');

// add products by admin
router.post('/addproduct', auth.verify, (req,res) => {
	const data = {userId: auth.decode(req.headers.authorization).id}
	productController.addProduct(data, req.body).then(resultFromController => res.send(resultFromController));
});

// all products by admin
router.get('/all', auth.verify, (req,res) => {
	const data = {userId: auth.decode(req.headers.authorization).id}
	productController.allProducts(data).then(resultFromController => res.send(resultFromController));
});

// retrieve active products
router.get('/active', (req,res) => {
	productController.activeProduct().then(resultFromController => res.send(resultFromController));
});

// retrieve speficific product
router.get('/:productId', (req,res) => {
	productController.singleProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// update product by admin
router.patch('/update/:productId', auth.verify, (req,res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.params.productId,
		productName: req.body.productName,
		description: req.body.description,
		price: req.body.price
	}
	productController.updateProduct(data).then(resultFromController => res.send(resultFromController));
});


// archive product by admin
router.patch('/archive/:productId', auth.verify, (req,res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.params.productId
	}
	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;