const mongoose = require('mongoose');

const cartSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, 'User ID is required!']
	},
	email: {
		type: String,
		required: [true, 'User ID is required!']
	},
	productId: {
		type: String,
		required: [true, 'Product ID is required']
	},
	productName: {
		type: String,
		required: [true, 'User ID is required!']
	},
	quantity: {
		type: Number,
		default: 1
	},
	isAdded: {
		type: Boolean,
		default: true
	},
	subTotal: {
		type: Number
	}
})

module.exports = mongoose.model('Cart', cartSchema);