const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, 'User ID is required!']
	},
	email: {
		type: String,
		required: [true, 'Email is required!']
	},
	products: [{
		productId: {
			type: String,
			required: [true, 'Product ID is required']
		},
		productName: {
			type: String,
			required: [true, 'Product Name is required!']
		},
		quantity: {
			type: Number,
			default: 1
		}
	}],
	totalAmount: {
		type: Number
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model('Order', orderSchema);